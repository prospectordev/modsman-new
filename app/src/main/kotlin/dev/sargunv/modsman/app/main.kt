package dev.sargunv.modsman.app

import javafx.application.Application

fun main() {
    Application.launch(MainApp::class.java)
}
